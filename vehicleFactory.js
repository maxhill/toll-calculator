const typesTollFree = {
  MOTORBIKE: "motorbike",
  TRACTOR: "tractor",
  EMERGENCY: "emergency",
  DIPLOMAT: "diplomat",
  FOREIGN: "foreign",
  MILITARY: "military"
};
const types = { CAR: "car", ...typesTollFree };

const create = ({ type }) => ({
  type,
  isTollFree: Object.values(typesTollFree).includes(type)
});

module.exports = {
  types,
  typesTollFree,
  create: type => create({ type }),
  createCar: () => create({ type: types.CAR }),
  createMotorbike: () => create({ type: types.MOTORCYCLE }),
  createTractor: () => create({ type: types.TRACTOR }),
  createEmergency: () => create({ type: types.EMERGENCY }),
  createDiplomat: () => create({ type: types.DIPLOMAT }),
  createForeign: () => create({ type: types.FOREIGN }),
  createMilitary: () => create({ type: types.MILITARY })
};
