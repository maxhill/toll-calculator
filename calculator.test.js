const vehicle = require("./vehicleFactory");
const calculateFee = require("./calculator");

const date = {
  monday: time => new Date(`2019-08-26 ${time}`),
  tuesday: time => new Date(`2019-08-27 ${time}`),
  wednesday: time => new Date(`2019-08-28 ${time}`),
  thursday: time => new Date(`2019-08-29 ${time}`),
  friday: time => new Date(`2019-08-30 ${time}`),
  saturday: time => new Date(`2019-08-31 ${time}`),
  sunday: time => new Date(`2019-09-01 ${time}`),
  holidays: [
    new Date(`2019-01-01`),
    new Date(`2019-03-28`),
    new Date(`2019-03-29`),
    new Date(`2019-04-1`),
    new Date(`2019-04-30`),
    new Date(`2019-05-01`),
    new Date(`2019-05-08`),
    new Date(`2019-05-09`),
    new Date(`2019-06-05`),
    new Date(`2019-06-06`),
    new Date(`2019-06-21`),
    new Date(`2019-07-01`),
    new Date(`2019-11-01`),
    new Date(`2019-12-24`),
    new Date(`2019-12-25`),
    new Date(`2019-12-26`),
    new Date(`2019-12-31`)
  ]
};

test("Returns a fee of 0 if there are no dates", () => {
  expect(calculateFee(vehicle.createCar())([])).toBe(0);
});

test("Returns a fee of 0 if the vehicle is toll-free", () => {
  Object.values(vehicle.typesTollFree).forEach(type => {
    expect(calculateFee(vehicle.create(type))([date.wednesday("17:00")])).toBe(
      0
    );
  });
});

test("Removes weekends", () => {
  expect(
    calculateFee(vehicle.createCar())([date.saturday(), date.sunday()])
  ).toBe(0);
});

test("Removes holidays", () => {
  expect(calculateFee(vehicle.createCar())(date.holidays)).toBe(0);
});

test("Calculates the right fee for the time of day", () => {
  const carFee = calculateFee(vehicle.createCar());

  expect(carFee([date.wednesday("5:00")])).toBe(0);
  expect(carFee([date.wednesday("6:00")])).toBe(8);
  expect(carFee([date.wednesday("6:30")])).toBe(13);
  expect(carFee([date.wednesday("7:00")])).toBe(18);
  expect(carFee([date.wednesday("8:00")])).toBe(13);
  expect(carFee([date.wednesday("8:30")])).toBe(8);
  expect(carFee([date.wednesday("14:59")])).toBe(8);
  expect(carFee([date.wednesday("15:00")])).toBe(13);
  expect(carFee([date.wednesday("15:30")])).toBe(18);
  expect(carFee([date.wednesday("17:00")])).toBe(13);
  expect(carFee([date.wednesday("18:00")])).toBe(8);
  expect(carFee([date.wednesday("18:30")])).toBe(0);
});

test("Sums up fees", () => {
  const carFee = calculateFee(vehicle.createCar());

  expect(
    carFee([
      date.wednesday("6:00"), // 8kr
      date.wednesday("8:00"), // 13kr
      date.wednesday("15:30"), //18kr
      date.wednesday("17:00") // 13kr
    ])
  ).toBe(52);
});

test("Removes fees that's within one hour of eachother", () => {
  const carFee = calculateFee(vehicle.createCar());
  expect(
    carFee([
      date.wednesday("6:00"), // 8kr
      date.wednesday("6:30"), // 13kr - should be removed
      date.wednesday("7:10") // 18kr - should not be removed
    ])
  ).toBe(26);
});

test("Sorts the dates", () => {
  const carFee = calculateFee(vehicle.createCar());
  expect(
    carFee([
      date.wednesday("7:10"), // 18kr - should not be removed
      date.wednesday("6:30"), // 13kr - should be removed
      date.wednesday("6:00") // 8kr
    ])
  ).toBe(26);
});
