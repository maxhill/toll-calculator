"use strict";
const calculator = require("./calculator");

module.exports.calculateToll = (event, context, callback) => {
  var body = JSON.parse(event.body);
  const vehicle = body.vehicle;
  const dates = body.dates.map(date => new Date(date));

  const response = {
    statusCode: 200,
    headers: {
      "Access-Control-Allow-Origin": "*" // Required for CORS support to work
    },
    body: JSON.stringify({
      cost: calculator(vehicle)(dates)
    })
  };

  callback(null, response);
};
