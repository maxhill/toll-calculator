/**
 * Currying function for calculating fee for a vehicle and dates.
 * @param {*} vehicle
 */
const calculateFee = vehicle => dates => {
  if (vehicle.isTollFree) return 0;

  return dates
    .sort((a, b) => sortAsc(a, b))
    .filter(date => removeWeekends(date))
    .filter(date => removeHolidays(date))
    .reduce((dates, current) => removeDatesWithinSameHour(dates, current), [])
    .map(date => addFee(date))
    .reduce((fee, entry) => fee + entry.fee, 0);
};

/**
 * Sort arrays of dates ascending.
 * @param {dateTime} a
 * @param {dateTime} b
 */
const sortAsc = (a, b) => a.getTime() - b.getTime();

/**
 * Use in array-filter to filter out dates that are weekends.
 * @param {dateTime} date
 */
const removeWeekends = date => {
  return date.getDay() < 6;
};

/**
 * Use in array-filter to filter out dates that are holidays.
 * @param {dateTime} date
 */
const removeHolidays = date => {
  const month = date.getMonth() + 1; // Make january 1 and not 0
  const day = date.getDate();
  if (
    (month == 1 && day == 1) ||
    (month == 3 && (day == 28 || day == 29)) ||
    (month == 4 && (day == 1 || day == 30)) ||
    (month == 5 && (day == 1 || day == 8 || day == 9)) ||
    (month == 6 && (day == 5 || day == 6 || day == 21)) ||
    month == 7 ||
    (month == 11 && day == 1) ||
    (month == 12 && (day == 24 || day == 25 || day == 26 || day == 31))
  ) {
    return false;
  }
  return true;
};

/**
 * Reduce array of dateTines to remove entris that
 * are within one hour of eachother.
 * @param {dateTime[]} dates
 * @param {dateTime} current
 */
const removeDatesWithinSameHour = (dates, current) => {
  const HOUR = 1000 * 60 * 60;
  const currentHourStart = current - HOUR;

  if (dates[dates.length - 1] > currentHourStart) {
    return dates;
  }

  dates.push(current);
  return dates;
};

/**
 *  Maps a date to its corresponding fee based on the time of day.
 * @param {dateTime} date
 */
const addFee = date => {
  const hour = date.getHours();
  const minute = date.getMinutes();

  if (hour < 6) return { date, fee: 0 }; // Before 06:00 = 0
  if (hour < 7 && minute < 30) return { date, fee: 8 }; // 06:00 -> 06:29 = 8
  if (hour < 7) return { date, fee: 13 }; // 06:30 -> 06:59 = 13
  if (hour < 8) return { date, fee: 18 }; // 07:00 -> 07:59 = 18
  if (hour < 9 && minute < 30) return { date, fee: 13 }; // 08:00 -> 08:29 = 13
  if (hour < 15) return { date, fee: 8 }; // 08:30 -> 14:59: = 8
  if (hour < 16 && minute < 30) return { date, fee: 13 }; // 15:00 -> 15:29 = 13
  if (hour < 17) return { date, fee: 18 }; // 15:30 -> 16:59 = 18
  if (hour < 18) return { date, fee: 13 }; // 17:00 -> 17:59 = 13
  if (hour < 19 && minute < 30) return { date, fee: 8 }; // 18:00 -> 18:29 = 8
  return { date, fee: 0 };
};

module.exports = calculateFee;
